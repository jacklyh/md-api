**General**
----
  _Api return general data of availability, country of residents, education, employment_status, languages, maritial_status, nationality, no_of_child, offday, skills, year_of_exp._

  _Updates 2020-09-15, Add general, household_size, housing_type, religion, user_type_

* **URL:** _/api/general_

* **Method:** `GET`

**Params:** _N/A_

**Success Response:**

```json
{
    "status" : "success",
    "skills": [
        {
          "id" : "1",
          "name": "Newborn"
        },
        {
          "id" : "2",
          "name": "Teen"
        },
        {
          "..."
        }
    ],
    "languages": [
        {
          "id" : "1",
          "name": "English"
        },
        {
          "id" : "2",
          "name": "Mandarin"
        },
        {
          "..."
        }
    ],
    "country_residence": [
        {
          "id" : "1",
          "name": "Thailand"
        },
        {
          "id" : "2",
          "name": "Indonesia"
        },
        {
          "..."
        }
    ],
    "type_of_housing": [
        {
          "id" : "1",
          "name": "HDB 4 rooms"
        },
        {
          "id" : "2",
          "name": "Condominium"
        },
        {
          "..."
        }
    ],
    "household_size": [
        {
          "id" : "1",
          "name": "1"
        },
        {
          "id" : "2",
          "name": "2"
        },
        {
          "..."
        }
    ],
    "nationality": [
        {
          "id" : "1",
          "name": "Thailand"
        },
        {
          "id" : "2",
          "name": "Indonesia"
        },
        {
          "..."
        }
    ],
    "year_of_exp": [
        {
          "id" : "1",
          "name": "1"
        },
        {
          "id" : "2",
          "name": "2"
        },
        {
          "..."
        }
    ],

}
```