**Update User Other skills**
----
  _Update user Other skills._

* **URL:** _/api/auth/other-skills_

* **Method:** `POST`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| skills             | Yes      | array {skill1, skill2, skill3} |

_Please take note you need to submit new and old skills together for create and update_

**Sample Data:**

```json
{
    "skills": [
        "infant care",
        "cook chinese food"
    ]
}
```

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "message": [
      "Other skills updated successfully"
    ]
  }
}
```


**Error Response:**

```json
{
  "status": "fail",
  "data": {
    "message": [
      "The skills field is required."
    ]
  }
}
```