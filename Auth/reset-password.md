**Reset Password**
----
  Reset Password api._

* **URL:** _/api/auth/reset-password_

* **Method:** `POST`

* **Authorization: Bearer <access_token>**

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| password           | Yes      | alphanumeric                |
| password_confirmation| Yes      | alphanumeric                |

**Sample Data:**

**Authorization: Bearer <access_token>**
```json
{
    "new_password": "909011)(*&^",
    "password_confirmation": "909011)(*&^",,
}
```

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "message": [
      "Password updated successfully"
    ]
  }
}
```


**Error Response:**

```json
{
  "status": "fail",
  "data": {
    "message": [
      "The password confirmation does not match."
    ]
  }
}
```