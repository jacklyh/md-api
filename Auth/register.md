**Register**
----
  _Register api._

* **URL:** _/api/auth/register_

* **Method:** `POST`

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| username           | Yes      | alphanumeric - Email format |
| uuid               | Yes      | alphanumeric                |
| password           | Yes      | alphanumeric                |
| country_code       | No       | alphanumeric                |


**Sample Data:**

```json
{
    "username": "johndoe@gmail.com",
    "uuid": "e0259bf8-b439-4e27-945f-58556413914f",
    "password": "12345!@#$"
}
```
_OR_
```json
{
    "username": "88889999",
    "uuid": "e0259bf8-b439-4e27-945f-58556413914f",
    "password": "12345!@#$",
}
```

**Success Response:**

```json
{
  "status": "success",
  "access_token": "1|V83kxJ2zOtyBLduDB5vyFDqqstX0iFhSEzLX9GKOH5EKRLIgIbQ2AKtjzYYadoF6AOt2HnAf0lcQYvVa",
  "token_type": "Bearer",
  "data": {
    "username": "johndoe@gmail.com"
  }
}
```


**Error Response:**

```json
{
  "status": "fail",
  "data": {
    "message": [
      "The username has already been taken."
    ]
  }
}
```


**Notes:**

  Do take note on the error response "_The username has already been taken._", please prompt user to login instead. Others response will stay in the register screen and show error(s).
