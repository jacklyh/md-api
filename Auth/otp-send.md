**OTP Send**
----
  _OTP in email / sms format, some use cases might require OTP such as new register, reset password_

* **URL:** _/api/auth/otp_

* **Method:** `GET`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

N/A

**Success Response:**

```json
{
  "status": "success"
}
```

**Error Response:**

```json
{
  "message": "Unauthenticated."
}
```