**OTP Verify**
----
  _OTP verification

* **URL:** _/api/auth/otp_

* **Method:** `POST`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| otp                | Yes      | 6 digits                    |


**Success Response:**

```json
{
  "status": "success"
}
```

**Error Response:**

```json
{
  "status": "fail",
  "data": {
    "message": "Invalid OTP service"
  }
}
{
  "status": "fail",
  "data": {
    "message": "Invalid OTP code"
  }
}
```