**Update User Photo**
----
  _Update user photo._

* **URL:** _/api/auth/photo_

* **Method:** `POST`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| photo              | Yes      | File, Type: Jpeg, Png, webp, Size: 1Mb |

**Sample Data:**

```json
{
    "photo": "photo file content",
}
```

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "photo": "https:\/\/md-users.s3.ap-southeast-1.amazonaws.com\/1\/aI3s7X2MsRBZ8twVQjCJZyj3IGJx9geiwRrr9i01.png"
  }
}
```


**Error Response:**

```json
{
  "status": "fail",
  "data": {
    "message": [
      "The photo must be a file of type: jpeg, png, jpg, gif, webp."
    ]
  }
}
```