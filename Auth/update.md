**Update Profile**
----
  Update helper profile api.

* **URL:** _/api/auth/profile_

* **Method:** `POST`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

| Name               | Required - Step | Type                        | Profile Step |
| -------------------|-----------------|-----------------------------|--------------|
| profile_step       | Yes             | alphanumeric (1,2,3,*)      | All Steps    |
| **Profile Step 1** |
| user_type          | Yes - 1         | integer                     | 1 |
| **Profile Step 2** |
| given_name         | Yes - 2         | alphanumeric                | 2 & * |
| family_name        | Yes - 2         | alphanumeric                | 2 & * |
| nationality        | Yes - 2         | integer                     | 2 & * |
| dob                | Yes - 2         | date e.g 2008-09-28         | 2 & * |
| religion           | Yes - 2         | integer                     | 2 & * |
| availability       | Yes - 2         | integer                     | 2 & * |
| employment_status  | Yes - 2         | integer                     | 2 & * |
| country_residence  | Yes - 2         | integer                     | 2 & * |
| postal_code        | Yes - 2         | integer                     | 2 & * |
| email              | No              | alphanumeric - Email format | 2 |
| contact_no         | No              | alphanumeric                | 2 |
| country_code       | No              | alphanumeric                | 2 |
| **Profile Step 3** |
| languages          | Yes - 3         | array {id1, id2, id3}       | 3 & * |
| year_of_exp        | Yes - 3         | integer                     | 3 & * |
| exp_in_sg          | Yes - 3         | integer                     | 3 & * |
| skills             | Yes - 3         | array {id1, id2, id3}       | 3 & * |
| handle_pork        | Yes - 3         | integer                     | 3 & * |
| eat_pork           | Yes - 3         | integer                     | 3 & * |
| **Others Profile Step (*)** |
| introduction       | No              | text                        | * |
| current_salary     | No              | integer                     | * |
| education          | No              | integer                     | * |
| offday             | No              | integer                     | * |
| maritial_status    | No              | integer                     | * |
| no_of_child        | No              | integer                     | * |
| prefer_str_date    | No              | date                        | * |
| prefer_household_size | No           | integer                     | * |
| prefer_housing_type   | No           | integer                     | * |

**Params Note:**
  _photo_ and _other_skills_ will be handle by seperate api call

**Sample Data:**

* **Profile Step 1**
```json
{
  "profile_step": 1,
  "user_type": 220,
}
```

* **Profile Step 2**
```json
{
  "profile_step": 2,
  "given_name": "Jj",
  "family_name": "Lim",
  "nationality": "5",
  "dob": "2008-09-28",
  "religion": "92",
  "availability": "50",
  "employment_status": "200",
  "country_residence": "300",
  "postal_code": "112233"
}
```

* **Profile Step 3**
```json
{
  "profile_step": 3,
  "languages": [
    32,
    35
  ],
  "year_of_exp": 29,
  "exp_in_sg": "210",
  "skills": [
    103,
    104
  ],
  "handle_pork": "211",
  "eat_pork": "211",
}
```

* **Profile All(*)**
```json
{
  "profile_step": "*",
  "given_name": "Doe",
  "family_name": "John",
  "dob": "2020-09-30",
  "nationality": "1",
  "religion": "92",
  "availability": "53",
  "employment_status": "200",
  "country_residence": "300",
  "postal_code": "119111",
  "year_of_exp": "29",
  "exp_in_sg": "210",
  "handle_pork": "211",
  "eat_pork": "211",
  "languages": [
    "35"
  ],
  "skills": [
    "104"
  ],
  "introduction": "This is the test intro",
  "current_salary": "1200",
  "education": "42",
  "offday": "60",
  "maritial_status": "71",
  "no_of_child": "85",
  "prefer_str_date": "2020-09-20",
  "prefer_household_size": "230",
  "prefer_housing_type": "240"
}
```

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "message": "Profile updated successfully",
    "profile_step": "1"
  }
}
```


**Error Response:**

```json
{
    "status" : "fail",
    "data": [
        {
          "message": [
            "Given name is required"
          ]

        },
    ]
}
```

```json
{
  "status": "fail",
  "data": {
    "message": [
      "Invalid request!"
    ]
  }
}
```


* **Notes:**

  <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._>
