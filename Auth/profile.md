**Profile**
----
  _Profile api usage on the wake up from mobile device or fresh launch._

* **URL:** _/api/auth/profile_

* **Method:** `GET`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

N/A

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "id": 1,
    "username": "88889999",
    "email": null,
    "given_name": "Jj",
    "family_name": "Lim",
    "contact_no": "88889999",
    "photo": null,
    "complete_percentage": 40,
    "user_type": 220,
    "referral_code": "zl8kl",
    "last_active_date": "2020-09-15 07:53PM",
    "created_at": "2020-09-15 03:33PM",
    "dob": "2008-09-28",
    "nationality": "5",
    "availability": "50",
    "year_of_exp": 29,
    "exp_in_sg": "210",
    "handle_pork": "211",
    "eat_pork": "211",
    "employment_status": "200",
    "religion": "92",
    "country_residence": "300",
    "postal_code": "112233",
    "introduction": null,
    "current_salary": null,
    "education": null,
    "offday": null,
    "maritial_status": null,
    "no_of_child": null,
    "prefer_str_date": null,
    "prefer_household_size": null,
    "prefer_housing_type": null,
    "languages": [
      32,
      35
    ],
    "skills": [
      103,
      104
    ],
    "other_skills": []
  }
}
```

**Error Response:**

```json
{
  "message": "Unauthenticated."
}
```