**Update User Type**
----
  _Update user type for helper / employer._

* **URL:** _/api/auth/profile-user-type_

* **Method:** `POST`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| user_type          | Yes      | Refer `general - user_type` |

**Sample Data:**

```json
{
    "user_type": "221",
}
```

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "message": "Profile User Type updated successfully"
  }
}
```


**Error Response:**
