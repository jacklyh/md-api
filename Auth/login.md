**Login**
----
  _Login api._

* **URL:** _/api/auth/login_

* **Method:** `POST`

**Params:**

| Name               | Required | Type                        |
| -------------------|----------|-----------------------------|
| username           | Yes      | alphanumeric - Email format |
| password           | Yes      | alphanumeric                |
| uuid               | Yes      | alphanumeric                |

**Sample Data:**

```json
{
    "email": "johndoe@gmail.com",
    "password": "12345!@#$",
    "uuid": "e0259bf8-b439-4e27-945f-58556413914f"
}
```

**Success Response:**

```json
{
  "status": "success",
  "access_token": "2|BB4IMNqTGZPEIKPrwYog9aM7Qt3nM95qSqAmOl7pPEZ8NhIlFmJHHzoNQyFr5m1FbQMEjXAtT7v6yB0y",
  "token_type": "Bearer",
  "data": {
    "username": "johndoe@gmail.com"
  }
}
```


**Error Response:**

```json
{
  "status": "fail",
  "data": {
    "message": [
      "Invalid Credentials"
    ]
  }
}
```