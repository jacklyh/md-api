**Employer Profile**
----
  _Employer Profile api usage on the wake up from mobile device or fresh launch._

* **URL:** _/api/auth/employer/profile_

* **Method:** `GET`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

N/A

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "id": 4,
    "username": "emp@gmail.com",
    "email": "emp@gmail.com",
    "given_name": "RY",
    "family_name": "Lai",
    "contact_no": null,
    "photo": null,
    "complete_percentage": 1,
    "user_type": 221,
    "referral_code": "gledm",
    "last_active_date": "2020-12-20 03:49PM",
    "created_at": "2020-12-20 03:13PM",
    "country_residence": "300",
    "district_code": "119111",
    "housing_type": "240",
    "household_size": 230,
    "has_elderly": "210",
    "has_pets": "210",
    "when_needed": "53",
    "request_nationality": "1",
    "request_year_of_exp": 29,
    "request_exp_in_sg": "210",
    "request_handle_pork": "211",
    "request_eat_pork": "211",
    "request_religion": "92",
    "request_note": "Good in take care of elderly",
    "request_offer_salary": "1200",
    "request_education": "42",
    "request_offday": "60",
    "request_maritial_status": "71",
    "request_no_of_child": "85",
    "languages": [
      35
    ],
    "skills": [
      104
    ],
    "other_skills": [
      "Wash Car",
      "take care of baby"
    ]
  }
}
```

**Error Response:**

```json
{
  "message": "Unauthenticated."
}
```