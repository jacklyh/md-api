**Update Employer Profile**
----
  Update employer profile api.

* **URL:** _/api/auth/employer/update-profile_

* **Method:** `POST`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

| Name               | Required | Type                        | General Ref |
| -------------------|----------|-----------------------------|--------------|
| given_name         | -         | alphanumeric                | - |
| family_name        | -         | alphanumeric                | - |
| country_residence  | -         | integer                     | country_residence |
| district_code      | -         | integer                     | - |
| household_size     | -         | integer                     | household_size |
| housing_type       | -         | integer                     | housing_type |
| has_elderly        | -         | integer                     | general |
| has_pets           | -         | integer                     | general |
| when_needed        | -         | integer                     | availability |
| request_nationality | -         | integer                     | nationality |
| request_year_of_exp | -         | integer                     | year_of_exp |
| request_exp_in_sg   | -         | integer                     | exp_in_sg |
| request_handle_pork | -         | integer                     | general |
| request_eat_pork    | -         | integer                     | general |
| request_religion   | -         | integer                     | religion |
| request_note       | -         | text                        | - |
| request_offer_salary | -         | integer                   | - |
| request_education | -         | integer                     | education |
| request_offday    | -         | integer                     | offday |
| request_maritial_status | -   | integer                     | maritial_status |
| request_no_of_child | -       | integer                     | no_of_child |
| skills             | -         | array {id1, id2, id3}       | skills |
| languages          | -         | array {id1, id2, id3}       | language |


| prefer_str_date    | -         | date                        | * |

**Params Note:**
  _photo_ and _other_skills_ will be handle by seperate api call

**Sample Data:**

```json
{
  "given_name": "RY",
  "family_name": "Lai",
  "country_residence": 300,
  "district_code": "119111",
  "household_size": 230,
  "housing_type": 240,
  "has_elderly": 210,
  "has_pets": 210,
  "when_needed": 53,
  "request_nationality": 1,
  "request_year_of_exp": 29,
  "request_exp_in_sg": 210,
  "request_handle_pork": 211,
  "request_eat_pork": 211,
  "request_religion": 92,
  "request_note": "Good in take care of elderly",
  "request_offer_salary": 1200,
  "request_education": 42,
  "request_offday": 60,
  "request_maritial_status": 71,
  "request_no_of_child": 85,
  "languages": [
    35
  ],
  "skills": [
    104
  ]
}
```

**Success Response:**

```json
{
  "status": "success",
  "data": {
    "message": "Profile updated successfully",
    "profile_step": "1"
  }
}
```


**Error Response:**

```json
{
    "status" : "fail",
    "data": [
        {
          "message": [
            "Given name is required"
          ]

        },
    ]
}
```

```json
{
  "status": "fail",
  "data": {
    "message": [
      "Invalid request!"
    ]
  }
}
```
