**Logout**
----
  _Logout api._

* **URL:** _/api/auth/logout_

* **Method:** `GET`

**Header:**

* **Authorization: Bearer <access_token>**
* **Accept: application/json**

**Params:**

N/A


**Success Response:**

```json
{
  "status": "success"
}
```