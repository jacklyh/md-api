**List of availables API:**
----
  Host: http://mdbackendapi-env.eba-rkffq33w.ap-southeast-1.elasticbeanstalk.com


**General**
----
* [General](general.md)
* [Register](Auth/register.md)
* [Login](Auth/login.md)

**Common Auth API**
----
* [Reset Password](Auth/reset-password.md)
* [Logout](Auth/logout.md)
* [Send / Resend OTP](Auth/otp-send.md)
* [Verify OTP](Auth/otp-verify.md)
* [Update Photo](Auth/update-photo.md)
* [Update User Type](Auth/update-profile-user-type.md) - **New**

**Helper API**
----
* [Profile](Auth/profile.md)
* [Update Helper Profile](Auth/update.md)
* [Update Other Skills](Auth/other-skills.md)

**Employer API**
----
* [Profile](Auth/Employer/profile.md) - **New**
* [Update Employer Profile](Auth/Employer/update-profile.md) - **New**
* [Update Employer Other Skills](Auth/Employer/other-skills.md) - **New**


**Json Response**
----
| Name               | Value                                  |
| -------------------|----------------------------------------|
| status             | This represents the error type. OK or 200 is success and rest everthing is failed.|
| code               | This represents the http code. This value is optional. Not be avaliable in all responses.|
| message            | A human-readable description of the error. You can provide your users with this information to indicate what they can do about the error.|
| data               | Success response data. |


**HTTP STATUS CODES**
----
  _We uses standard HTTP status codes to indicate success or failure of an API request. Codes in the 2xx range indicate that a request was successfuly processed and codes in the 4xx range indicate that there was an error that resulted from the information sent (e.g. authentication, no balance or a missing or wrong parameter)._

  _In case of an error, the body of the response includes a JSON formatted response that tells you exactly what is wrong._

| Code               | Description                            |
| -------------------|----------------------------------------|
| 200                | OK - Everything went as planned.       |
| 202                | Accepted - Request accepted.           |
| 400                | Bad Request - Something in your header or request body was malformed.       |
| 401                | Unauthorised - Necessary credentials were either missing or invalid.        |
| 403                | Your credentials are valid, but you don’t have access to the requested resource. |
| 404                | The resources cannot be found.       |
| 409                | Conflict - You might be trying to update the same resource concurrently.    |
| 422                | Validation error       |
| 429                | Too Many Requests - You are calling our APIs more frequently than we allow.       |
| 5xx                | Something went wrong on our end. Please try again.       |
